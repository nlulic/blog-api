# ✏️blog-api

A simple blog API built with Go that uses the GitLab API to retrieve issues based on their label and turn them into blog posts.

## Why

I was looking for a small web project I can build with Go, and since I've thought about adding a blog to my homepage for a long time, I've decided to create a small blog API.
I use GitLab as a repository, pipeline and hosting (GitLab Pages) for my homepage, so it made sense to me to keep everything together and also (miss-)use GitLab to manage my future blog posts (I also wanted to built something quick and avoid dealing with authentication and building the UI for creating posts...).

My first attempt was to use the Wiki for managing blog posts. However, the API didn't support sorting the posts by creation date. Also, it didn't provide any information about when the wiki entries were created, so sorting the Wiki myself wouldn't work (at least not without manually adding metadata to every post and parsing text every time when retrieving the Wiki entries).

I've decided to use the Issues API, which offered a more sophisticated API with already built-in pagination and the possibility to filter and sort issues.

## Limitations

- It is currently not possible to display images in posts.
- The GitLab API enforces a rate limit of 2000 requests per minute (this should be fine for small blogs)
- The posts can only be fetched individually by the issue id. Retrieving an issue by a slug, like it is common for most blogs, is not possible.

## Conclusion 

This `blog-api` is a project to get my feet wet with Go. It will be used as a temporary solution (or expanded until all limitations are bypassed) and should not be used for blogs that expect to gain significant traffic.

### TODOs

- tests
- display images when retrieving individual posts
- cache requests

## Running the App

- rename the .env.sample to .env
- adjust the .env file (or use environment variables)
- make run - to run the app locally
- make build - to create an executable