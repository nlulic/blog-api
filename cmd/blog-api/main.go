package main

import (
	"log"

	"gitlab.com/nlulic/blog-api/internal/platform/cmd"
	"gitlab.com/nlulic/blog-api/internal/storage/sqlite"
)

var version = ""

func main() {

	storage := sqlite.NewSqliteConn(sqlite.SqliteConfig{"blog-api.db"})

	if err := cmd.RunCmd(cmd.CmdContext{storage, version}); err != nil {
		log.Fatal(err)
	}
}
