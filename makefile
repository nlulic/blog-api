VERSION := $(shell git describe --always --tags --dirty)

OUT_FILE := blog-api

ROOT_PKG := gitlab.com/nlulic/blog-api
BLOG_API = cmd/blog-api

run: 
	go run ${ROOT_PKG}/${BLOG_API}

build:
	go build -i -v -o ${OUT_FILE} -ldflags="-X main.version=${VERSION}" ${ROOT_PKG}/${BLOG_API}

# requires Uplink (https://getuplink.de/) to be present in PATH
uplink: 
	uplink serve --addr :8080
