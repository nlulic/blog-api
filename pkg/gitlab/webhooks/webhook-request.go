package webhooks

import (
	"gitlab.com/nlulic/blog-api/pkg/gitlab"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/client"
)

type webhookRequest struct {
	User             gitlab.User `json:"user"`
	ObjectAttributes struct {
		Id          int    `json:"iid"`
		Title       string `json:"title"`
		Description string `json:"description"`
		AssigneeId  int    `json:"assignee_id"`
		AuthorId    int    `json:"author_id"`
		Labels      []struct {
			Title string `json:"title"`
		} `json:"labels"`
		Confidential bool   `json:"confidential"`
		State        string `json:"state"`
		CreatedAt    string `json:"created_at"`
		ClosedAt     string `json:"closed_at"`
		UpdatedAt    string `json:"updated_at"`
		Action       string `json:"action"`
	} `json:"object_attributes"`
	Assignees []gitlab.User `json:"assignees"`
}

// getLabels returns a slice of strings for the labels of a `webhookRequest`
func (wh *webhookRequest) getLabels() []string {

	var labels []string

	for _, v := range wh.ObjectAttributes.Labels {
		labels = append(labels, v.Title)
	}

	return labels
}

// getAuthor returns the issue author
func (wh *webhookRequest) getAuthor(c *client.GitlabClient) (*gitlab.User, error) {

	if wh.ObjectAttributes.AuthorId == wh.User.Id {
		return &wh.User, nil
	}

	return c.GetUser(wh.ObjectAttributes.AuthorId)
}

// getCreatedAt returns the `CreatedAt` field for the `Post`. If the `ClosedAt` (the time on
// which a post should be published) is not present, the `CreatedAt` is used instead
func (wh *webhookRequest) getCreatedAt() string {

	createdAt := wh.ObjectAttributes.ClosedAt

	if createdAt == "" {
		createdAt = wh.ObjectAttributes.CreatedAt
	}

	return createdAt
}
