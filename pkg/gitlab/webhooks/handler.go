package webhooks

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/nlulic/blog-api/internal/domain"
	"gitlab.com/nlulic/blog-api/internal/storage/context"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/client"
	timeUtils "gitlab.com/nlulic/blog-api/pkg/utils/time"
)

func Handler(apiClient *client.GitlabClient, secret string) http.HandlerFunc {

	const ISSUE_HOOK = "Issue Hook"

	return isAuthorized(func(w http.ResponseWriter, r *http.Request) {

		if r.Header.Get("X-Gitlab-Event") != ISSUE_HOOK {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		body, _ := ioutil.ReadAll(r.Body)
		defer r.Body.Close()

		wh := webhookRequest{}

		if err := json.Unmarshal(body, &wh); err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		s := context.GetStorage(r.Context())

		if !shouldPublish(wh.ObjectAttributes.State, wh.ObjectAttributes.Confidential, wh.getLabels(), apiClient.Labels) {
			if err := s.DeletePost(wh.ObjectAttributes.Id); err != nil {
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			return
		}

		author, err := wh.getAuthor(apiClient)

		post := &domain.Post{
			Id:        wh.ObjectAttributes.Id,
			Title:     wh.ObjectAttributes.Title,
			Content:   wh.ObjectAttributes.Description,
			CreatedAt: timeUtils.FormatUtcToRFC3339(wh.getCreatedAt()),
			UpdatedAt: timeUtils.FormatUtcToRFC3339(wh.ObjectAttributes.UpdatedAt),
			Author:    domain.Author(*author),
		}

		err = s.CreateOrUpdatePost(post)

		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
	}, secret)
}
