package webhooks

import "net/http"

func isAuthorized(next http.HandlerFunc, secret string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if secret != r.Header.Get("X-Gitlab-Token") {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		next(w, r)
	}
}
