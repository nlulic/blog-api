package webhooks

import (
	"strings"

	"gitlab.com/nlulic/blog-api/pkg/gitlab/client"
)

// shouldPublish checks whether the updated issue should be published (persisted in the storage)
func shouldPublish(state string, confidential bool, labels []string, requiredLabels string) bool {

	if state != client.STATUS_CLOSED {
		return false
	}

	// TODO: "SHOW_CONFIDENTIAL" should be configurable
	// ignores confidential issues
	if !client.SHOW_CONFIDENTIAL && confidential {
		return false
	}

	labelMap := make(map[string]bool)
	for _, v := range strings.Split(requiredLabels, ",") {
		labelMap[v] = true
	}

	// checks whether there is at least one label matching the configured "blog" labels
	for _, label := range labels {
		_, ok := labelMap[label]

		if ok {
			return ok
		}
	}

	return false
}
