package client

import "net/http"

type GitlabClient struct {
	client *http.Client
	*GitlabApiConfig
}

type GitlabApiConfig struct {
	ApiUrl    string
	ApiKey    string
	ProjectId string
	Labels    string
}

func NewGitlabClient(conf GitlabApiConfig) *GitlabClient {

	ok := checkConf(conf)

	if !ok {
		panic("value(s) missing in gitlab config")
	}

	return &GitlabClient{
		&http.Client{},
		&conf,
	}
}

func checkConf(conf GitlabApiConfig) bool {

	if conf.ApiUrl == "" ||
		conf.ApiKey == "" ||
		conf.ProjectId == "" ||
		conf.Labels == "" {
		return false
	}

	return true
}
