package client

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/nlulic/blog-api/pkg/gitlab"
	"gitlab.com/nlulic/blog-api/pkg/utils"
)

func (g *GitlabClient) GetUser(userId int) (*gitlab.User, error) {

	url, err := utils.NewUrlBuilder(g.ApiUrl).Url("users", strconv.Itoa(userId)).Value()

	if err != nil {
		return nil, err
	}

	resp, err := g.makeRequest(http.MethodGet, url)

	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		return nil, err
	}

	user := &gitlab.User{}

	if err := json.Unmarshal(body, user); err != nil {
		return nil, err
	}

	return user, nil
}
