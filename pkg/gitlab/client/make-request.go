package client

import "net/http"

func (g *GitlabClient) makeRequest(method string, url string) (*http.Response, error) {

	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+g.ApiKey)

	resp, err := g.client.Do(req)

	if err != nil {
		return nil, err
	}

	return resp, nil
}
