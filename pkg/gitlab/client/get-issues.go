package client

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/nlulic/blog-api/pkg/gitlab"
	"gitlab.com/nlulic/blog-api/pkg/utils"
)

// GetIssues retrieves all issues from the GitLab API regardless the pagination
func (g *GitlabClient) GetIssues(query gitlab.IssuesQuery) (*[]gitlab.Issue, error) {

	issues, pagination, err := g.fetchIssues(query)

	if err != nil {
		return nil, err
	}

	resc, errc := make(chan []gitlab.Issue), make(chan error)

	next, total := pagination.NextPage, pagination.TotalPages

	// TODO: log progress
	for next != 0 && next <= total {

		next++
		query.Page++

		go func(query gitlab.IssuesQuery) {

			issues, _, err := g.fetchIssues(query)

			if err != nil {
				errc <- err
			}

			resc <- issues

		}(query)
	}

	for nextPage := pagination.NextPage; nextPage != 0 && nextPage <= pagination.TotalPages; nextPage++ {
		select {
		case result := <-resc:
			issues = append(issues, result...)
		case err := <-errc:
			return nil, err
		}
	}

	return &issues, nil
}

// fetchIssues retrieves the issues via the GitLab API
func (g *GitlabClient) fetchIssues(query gitlab.IssuesQuery) ([]gitlab.Issue, *gitlab.Pagination, error) {

	url, err := utils.NewUrlBuilder(g.ApiUrl).Url("projects", g.ProjectId, "issues").WithQuery(query).Value()

	if err != nil {
		return nil, nil, err
	}

	resp, err := g.makeRequest(http.MethodGet, url)

	if err != nil {
		return nil, nil, err
	}

	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	if err != nil {
		return nil, nil, err
	}

	issues := &[]gitlab.Issue{}

	if err := json.Unmarshal(body, issues); err != nil {
		return nil, nil, err
	}

	return *issues, getPagination(resp), nil
}

// getPagination returns the pagination from the response headers
func getPagination(r *http.Response) *gitlab.Pagination {

	parse := func(headerName string) int {
		header := r.Header.Get(headerName)
		value, _ := strconv.Atoi(header)
		return value
	}

	return &gitlab.Pagination{
		Page:       parse("X-Page"),
		PrevPage:   parse("X-Prev-Page"),
		NextPage:   parse("X-Next-Page"),
		Total:      parse("X-Total"),
		TotalPages: parse("X-Total-Pages"),
	}
}
