package uploads

import "net/http"

func Handler(redirectUrl string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, redirectUrl+r.URL.String(), http.StatusMovedPermanently)
	}
}
