package gitlab

type Issue struct {
	Id           int      `json:"iid"`
	Title        string   `json:"title"`
	Description  string   `json:"description"`
	Author       User     `json:"author"`
	Assignee     User     `json:"assignee"`
	Assignees    []User   `json:"assignees"`
	Labels       []string `json:"labels"`
	Confidential bool     `json:"confidential"`
	State        string   `json:"state"`
	CreatedAt    string   `json:"created_at"`
	ClosedAt     string   `json:"closed_at"`
	UpdatedAt    string   `json:"updated_at"`
}

type User struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	AvatarUrl string `json:"avatar_url"`
	Username  string `json:"username"`
}

type IssuesQuery struct {
	Limit        int    `url:"per_page"`
	Page         int    `url:"page"`
	Labels       string `url:"labels"`
	State        string `url:"state"`
	Confidential bool   `url:"confidential"`
	OrderBy      string `url:"order_by"`
}

type Pagination struct {
	Page       int
	PrevPage   int
	NextPage   int
	Total      int
	TotalPages int
}
