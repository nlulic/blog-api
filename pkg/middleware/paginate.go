package middleware

import (
	"context"
	"net/http"
	"strconv"
)

type PagingQuery struct {
	PerPage int
	Page    int
}

const PAGING_KEY string = "paging"

func Paginate(perPage int) func(http.Handler) http.Handler {

	return func(next http.Handler) http.Handler {

		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			paging := &PagingQuery{perPage, 1}

			if v, ok := parseParam(r, "page"); ok {
				paging.Page = v
			}

			if v, ok := parseParam(r, "per_page"); ok {
				paging.PerPage = v
			}

			ctxt := context.WithValue(r.Context(), PAGING_KEY, *paging)

			next.ServeHTTP(w, r.WithContext(ctxt))
		})
	}
}

func parseParam(r *http.Request, param string) (int, bool) {

	str := r.URL.Query().Get(param)

	value, err := strconv.Atoi(str)

	if err != nil {
		return 0, false
	}

	return value, true
}
