// Package viperex provides extended viper functions to bind and get flags
// to allow the CLI flags to be provided in `kebab-case` while the config
// keys which are provided by an .env file can remain in `snake_case`.
package viperex

import (
	"strings"

	"github.com/spf13/pflag"
	vpr "github.com/spf13/viper"
)

// transforms `kebab-case` to `snake_case`
var snakeCaseReplacer = strings.NewReplacer("-", "_")

// reverts `snake_case` back to `kebab-case`
var kebabCaseReplacer = strings.NewReplacer("_", "-")

// GetBool returns the value associated with the key as a boolean while transforming the key to `snake_case`.
func GetBool(key string) bool {
	return vpr.GetBool(snakeCaseReplacer.Replace(key))
}

// GetString returns the value associated with the key as a string while transforming the key to `snake_case`.
func GetString(key string) string {
	return vpr.GetString(snakeCaseReplacer.Replace(key))
}

// Get returns an interface while transforming the key to `snake_case`.
func Get(key string) interface{} {
	return vpr.Get(snakeCaseReplacer.Replace(key))
}

// BindPFlags binds a full flag set to the configuration, using each normalized flag's long name as the config key e.g "api-base-url" will be normalized to "api_base_url".
func BindPFlags(flags *pflag.FlagSet) {

	// normalize to `snake_case` so the configured variables which are defined in `snake_case` get bound
	normalize(snakeCaseReplacer, flags)

	vpr.BindPFlags(flags)

	// revert the normalization after the values have been bound - this avoids for example the `--help` command
	// to display the flags in snake_case while keeping the viper binding
	normalize(kebabCaseReplacer, flags)
}

func normalize(replacer *strings.Replacer, flags *pflag.FlagSet) {

	normalize := flags.GetNormalizeFunc()

	flags.SetNormalizeFunc(func(fs *pflag.FlagSet, name string) pflag.NormalizedName {
		result := normalize(fs, name)
		name = replacer.Replace(string(result))

		return pflag.NormalizedName(name)
	})
}
