package utils

type OffsetPagination struct {
	Page       int
	Total      int
	TotalPages int
	Records    int
}

func NewOffsetPagination(page, perPage, count, records int) *OffsetPagination {

	totalPages := (count + perPage - 1) / perPage

	return &OffsetPagination{page, perPage, totalPages, records}
}
