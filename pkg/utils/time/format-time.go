package time

import "time"

// formatTime formats the timestamp from `"2006-01-02 15:04:05 UTC"` to `"2006-01-02T15:04:05Z07:00"`
func FormatUtcToRFC3339(timestamp string) string {

	const UTC_TIME = "2006-01-02 15:04:05 UTC"

	t, _ := time.Parse(UTC_TIME, timestamp)

	return t.Format(time.RFC3339)
}
