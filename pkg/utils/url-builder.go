package utils

import (
	"strings"

	"github.com/google/go-querystring/query"
)

type UrlBuilder struct {
	BaseUrl string
	url     string
	query   string
	err     error
}

func NewUrlBuilder(baseUrl string) *UrlBuilder {

	builder := UrlBuilder{
		BaseUrl: baseUrl,
	}

	return &builder
}

func (b *UrlBuilder) Url(segments ...string) *UrlBuilder {

	b.url = strings.Join(segments, "/")

	return b
}

func (b *UrlBuilder) WithQuery(q interface{}) *UrlBuilder {

	params, err := query.Values(q)

	if err != nil {
		b.err = err
		return b
	}

	b.query = params.Encode()

	return b
}

func (b *UrlBuilder) Value() (string, error) {

	if b.err != nil {
		return "", b.err
	}

	path := strings.Join([]string{b.BaseUrl, b.url}, "/")

	if b.query != "" {
		return path + "?" + b.query, nil
	}

	return path, nil
}
