module gitlab.com/nlulic/blog-api

go 1.14

require (
	github.com/go-chi/chi/v5 v5.0.2
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/google/go-querystring v1.1.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.7
	github.com/spf13/cobra v1.2.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.9.0
)
