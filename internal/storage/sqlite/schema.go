package sqlite

const CREATE_SCHEMA = `

	CREATE TABLE IF NOT EXISTS author (
		id INTEGER PRIMARY KEY,
		username VARCHAR(255) NOT NULL,
		avatar_url TEXT NOT NULL,
		name TEXT NOT NULL
	);

	CREATE INDEX IF NOT EXISTS idx_author_id ON author (id);

	CREATE TABLE IF NOT EXISTS post (
		id INTEGER PRIMARY KEY,
		title TEXT NOT NULL,
		content TEXT,
		created_at DATETIME,
		updated_at DATETIME, 
		author_id INTEGER,
		FOREIGN KEY(author_id) REFERENCES author(id)
	);

	CREATE INDEX IF NOT EXISTS idx_post_author_id ON post (id, author_id);
`

const INSERT_AUTHOR = `
	INSERT INTO author (id, username, avatar_url, name) 
		VALUES (?, ?, ?, ?) 
		ON CONFLICT (id) DO UPDATE SET 
		username=EXCLUDED.username,
		avatar_url=EXCLUDED.avatar_url,
		name=EXCLUDED.name;
`

const INSERT_POST = `
	INSERT INTO post (id, title, content, created_at, updated_at, author_id) 
		VALUES (?, ?, ?, ?, ?, ?) 
		ON CONFLICT (id) DO UPDATE SET 
			title=EXCLUDED.title,  
			content=EXCLUDED.content,  
			created_at=EXCLUDED.created_at,  
			updated_at=EXCLUDED.updated_at, 
			author_id=EXCLUDED.author_id;
`

const DELETE_POST = `DELETE FROM post WHERE id = ?;`

const SELECT_POSTS = `
	SELECT post.id, title, content, created_at, updated_at,
		author.id "author.id", 
		author.username "author.username", 
		author.avatar_url "author.avatar_url", 
		author.name "author.name"
	FROM post 
	JOIN author ON author_id = author.id
	ORDER BY created_at DESC
	LIMIT ? OFFSET ?
`

const SELECT_POSTS_COUNT = `SELECT COUNT() FROM post;`

const SELECT_POST = `
	SELECT post.id, title, content, created_at, updated_at,
		author.id "author.id", 
		author.username "author.username", 
		author.avatar_url "author.avatar_url", 
		author.name "author.name"
	FROM post
	JOIN author ON author_id = author.id
	WHERE post.id = ?
`
