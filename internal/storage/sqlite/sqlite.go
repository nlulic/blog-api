package sqlite

import (
	"database/sql"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type Sqlite struct {
	db    *sqlx.DB
	stmts map[int]*sql.Stmt
}

type SqliteConfig struct {
	FilePath string
}

func NewSqliteConn(config SqliteConfig) *Sqlite {

	db, err := sqlx.Connect("sqlite3", config.FilePath)

	if err != nil {
		log.Fatal(err)
	}

	db.MustExec(CREATE_SCHEMA)

	stmts := make(map[int]*sql.Stmt)

	conn := Sqlite{db, stmts}
	conn.prepare()

	return &conn
}
