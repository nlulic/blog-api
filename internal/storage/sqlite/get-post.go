package sqlite

import "gitlab.com/nlulic/blog-api/internal/domain"

func (sqlite *Sqlite) GetPost(id int) (*domain.Post, error) {

	post := &domain.Post{}

	err := sqlite.db.Get(post, SELECT_POST, id)

	if err != nil {
		return nil, err
	}

	return post, nil
}
