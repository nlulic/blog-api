package sqlite

func (sqlite *Sqlite) DeletePost(id int) error {

	stmt := sqlite.stmts[DeletePost]

	_, err := stmt.Exec(id)

	return err
}
