package sqlite

import "gitlab.com/nlulic/blog-api/internal/domain"

func (sqlite *Sqlite) CreateOrUpdatePost(post *domain.Post) error {

	stmtPost := sqlite.stmts[InsertPostStmt]
	stmtAuthor := sqlite.stmts[InsertAuthorStmt]

	tx, err := sqlite.db.Begin()

	if err != nil {
		return err
	}

	_, err = tx.Stmt(stmtAuthor).Exec(
		post.Author.Id,
		post.Author.Username,
		post.Author.AvatarUrl,
		post.Author.Name,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = tx.Stmt(stmtPost).Exec(
		post.Id,
		post.Title,
		post.Content,
		post.CreatedAt,
		post.UpdatedAt,
		post.Author.Id,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	err = tx.Commit()

	if err != nil {
		return err
	}

	return nil
}
