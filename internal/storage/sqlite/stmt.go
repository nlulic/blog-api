package sqlite

import (
	"log"
)

const (
	InsertAuthorStmt int = iota
	InsertPostStmt
	DeletePost
)

func (s *Sqlite) prepare() {

	statements := []struct {
		id    int
		value string
	}{
		{InsertAuthorStmt, INSERT_AUTHOR},
		{InsertPostStmt, INSERT_POST},
		{DeletePost, DELETE_POST},
	}

	for _, statement := range statements {

		stmt, err := s.db.Prepare(statement.value)

		if err != nil {
			log.Fatal(err)
		}

		s.stmts[statement.id] = stmt
	}
}
