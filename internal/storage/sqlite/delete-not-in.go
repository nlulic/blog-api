package sqlite

import (
	"fmt"
	"strings"
)

func (sqlite *Sqlite) DeleteNotIn(ids []int) error {

	sqlStmt := fmt.Sprintf("DELETE FROM post WHERE id NOT IN (%s);", join(ids))

	_, err := sqlite.db.Exec(sqlStmt)

	if err != nil {
		return err
	}

	return nil
}

func join(ids []int) string {
	return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(ids)), ","), "[]")
}
