package sqlite

import (
	"gitlab.com/nlulic/blog-api/internal/domain"
	"gitlab.com/nlulic/blog-api/pkg/utils"
)

func (sqlite *Sqlite) GetPosts(page, perPage int) ([]*domain.Post, *utils.OffsetPagination, error) {

	count, err := totalPosts(sqlite)

	if err != nil {
		return nil, nil, err
	}

	posts := make([]*domain.Post, 0)

	err = sqlite.db.Select(&posts, SELECT_POSTS, perPage, (page-1)*perPage)

	if err != nil {
		return nil, nil, err
	}

	pagination := utils.NewOffsetPagination(page, perPage, count, len(posts))

	return posts, pagination, nil
}

func totalPosts(sqlite *Sqlite) (int, error) {

	var count int

	row := sqlite.db.QueryRow(SELECT_POSTS_COUNT)

	err := row.Scan(&count)

	if err != nil {
		return 0, err
	}

	return count, nil

}
