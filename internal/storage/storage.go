package storage

import (
	"gitlab.com/nlulic/blog-api/internal/domain"
	"gitlab.com/nlulic/blog-api/pkg/utils"
)

type Storage interface {
	CreateOrUpdatePost(p *domain.Post) error
	DeletePost(id int) error
	DeleteNotIn(ids []int) error
	GetPosts(page int, perPage int) ([]*domain.Post, *utils.OffsetPagination, error)
	GetPost(id int) (*domain.Post, error)
}
