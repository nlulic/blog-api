package context

import (
	"context"

	"gitlab.com/nlulic/blog-api/internal/storage"
)

func SetStorage(c context.Context, s storage.Storage) context.Context {
	return context.WithValue(c, "storage", s)
}

func GetStorage(c context.Context) storage.Storage {
	return c.Value("storage").(storage.Storage)
}
