package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func version(version string) func(cmd *cobra.Command, args []string) {

	return func(cmd *cobra.Command, args []string) {

		if version != "" {
			fmt.Printf("Version: %s", version)
		}
	}
}
