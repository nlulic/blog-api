package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/nlulic/blog-api/internal/platform/server"
	"gitlab.com/nlulic/blog-api/internal/storage"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/client"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/uploads"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/webhooks"
	"gitlab.com/nlulic/blog-api/pkg/viperex"
)

func runApi(storage storage.Storage) func(cmd *cobra.Command, args []string) {

	return func(cmd *cobra.Command, args []string) {

		s := server.NewServer(server.ChiServerConfig{
			storage,
			server.DebugMode,
			viperex.GetString("cors"),
		})

		// register additional routes specific to the GitLab
		uploadsRoute(s)
		webhooksRoute(s)

		port := viperex.GetString("port")

		log.Println("Server started on port", port)
		err := s.Run(port)

		if err != nil {
			panic(err)
		}
	}
}

func uploadsRoute(s *server.ChiServer) {

	gitlabProjectUrl := viperex.GetString("api-project-url")

	if gitlabProjectUrl == "" {
		log.Println("GitLab project url is not set - \"/uploads\" handler will not be registered")
		return
	}

	uploadsFn := s.RegisterGetRoute("/uploads")
	uploadsFn("/{uuid}/{filename}", uploads.Handler(gitlabProjectUrl))
}

func webhooksRoute(s *server.ChiServer) {

	secret := viperex.GetString("webhook-secret")

	if secret == "" {
		log.Println("Webhook token is not set - make sure to secure the \"/webhook\" route in production")
	}

	webhook := s.RegisterPostRoute("/webhook")

	webhook("/", webhooks.Handler(makeGitlabClient(), secret))
}

func makeGitlabClient() *client.GitlabClient {

	gitlabConf := client.GitlabApiConfig{
		viperex.GetString("api-base-url"),
		viperex.GetString("api-key"),
		viperex.GetString("api-project-id"),
		viperex.GetString("api-blog-labels"),
	}

	return client.NewGitlabClient(gitlabConf)
}
