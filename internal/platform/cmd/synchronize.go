package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/nlulic/blog-api/internal/domain"
	"gitlab.com/nlulic/blog-api/internal/storage"
	"gitlab.com/nlulic/blog-api/pkg/gitlab"
	"gitlab.com/nlulic/blog-api/pkg/gitlab/client"
	"gitlab.com/nlulic/blog-api/pkg/viperex"
)

func synchronize(s storage.Storage) func(cmd *cobra.Command, args []string) {

	return func(cmd *cobra.Command, args []string) {

		if skip := viperex.GetBool("skip-sync"); skip {
			return
		}

		apiClient := makeGitlabClient()

		// get issues from gitlab
		issues, err := apiClient.GetIssues(gitlab.IssuesQuery{100, 1, apiClient.Labels, client.STATUS_CLOSED, client.SHOW_CONFIDENTIAL, client.ORDER_BY})

		if err != nil {
			panic(err)
		}

		posts := make([]*domain.Post, 0)
		ids := make([]int, 0)

		for _, issue := range *issues {
			posts = append(posts, toPost(&issue))
			ids = append(ids, issue.Id)
		}

		// insert/update fetched posts
		if err := insertPosts(posts, s); err != nil {
			panic(err)
		}

		// delete posts which have not been fetched from the api
		if err := s.DeleteNotIn(ids); err != nil {
			panic(err)
		}

		log.Println("Synchronized posts:", len(posts))
	}
}

func insertPosts(p []*domain.Post, s storage.Storage) error {

	errc := make(chan error)

	for _, post := range p {
		go func(post *domain.Post) {

			err := s.CreateOrUpdatePost(post)

			if err != nil {
				errc <- err
				return
			}

			errc <- nil

		}(post)
	}

	for _ = range p {
		select {
		case err := <-errc:
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func toPost(issue *gitlab.Issue) *domain.Post {

	author := issue.Author

	if issue.Assignee.Id != 0 {
		author = issue.Assignee
	}

	return &domain.Post{
		issue.Id,
		issue.Title,
		issue.Description,
		issue.ClosedAt,
		issue.UpdatedAt,
		domain.Author{
			author.Id,
			author.Name,
			author.AvatarUrl,
			author.Username,
		},
	}
}
