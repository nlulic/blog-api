package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/nlulic/blog-api/internal/storage"
	"gitlab.com/nlulic/blog-api/pkg/viperex"
)

type CmdContext struct {
	Storage storage.Storage
	Version string
}

func RunCmd(c CmdContext) error {

	rootCmd := &cobra.Command{
		Use:    "blog-api",
		PreRun: synchronize(c.Storage),
		Run:    runApi(c.Storage),
	}

	rootCmd.Flags().Bool("skip-sync", false, "skips the Issue synchronization")
	rootCmd.Flags().StringP("port", "p", "8080", "port on which the server should listen")
	rootCmd.Flags().StringP("cors", "c", "*", "allowed origin")

	// GitLab api
	rootCmd.Flags().String("api-base-url", "https://gitlab.com/api/v4", "base url of the GitLab api")
	rootCmd.Flags().String("api-key", "", "GitLab api key with read access to the API")
	rootCmd.Flags().String("api-project-id", "", "GitLab project id - required to build the api path")
	rootCmd.Flags().String("api-blog-labels", "Blog", "comma separated list of labels which mark an issue as a post")

	// Required to build the GitLab uploads path
	rootCmd.Flags().String("api-project-url", "", "base url of the project (used to display images in posts)")

	// Webhooks
	rootCmd.Flags().String("webhook-secret", "", "secret token to validated received webhook requests")

	// TODO: Mark missing flags required
	viperex.BindPFlags(rootCmd.Flags())

	versionCmd := &cobra.Command{
		Use:   "version",
		Short: "Show version",
		Run:   version(c.Version),
	}

	rootCmd.AddCommand(versionCmd)

	return rootCmd.Execute()
}

func init() {

	viper.AutomaticEnv()
	viper.SetEnvPrefix("blog_api")

	viper.SetConfigFile(".env")
	viper.ReadInConfig()
}
