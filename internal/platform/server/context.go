package server

import (
	"net/http"

	"gitlab.com/nlulic/blog-api/internal/storage"
	"gitlab.com/nlulic/blog-api/internal/storage/context"
)

func (s *ChiServer) context() {
	s.Router.Use(StorageCtx(s.Storage))
}

func StorageCtx(storage storage.Storage) func(next http.Handler) http.Handler {

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctxt := context.SetStorage(r.Context(), storage)
			next.ServeHTTP(w, r.WithContext(ctxt))
		})
	}
}
