package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/nlulic/blog-api/internal/storage"
)

type ChiServer struct {
	Router *chi.Mux
	ChiServerConfig
}

type ChiServerConfig struct {
	Storage       storage.Storage
	Mode          ChiServerMode
	AllowedOrigin string
}

type ChiServerMode int

const (
	DebugMode ChiServerMode = iota
	ReleaseMode
)

func NewServer(config ChiServerConfig) *ChiServer {

	s := ChiServer{
		chi.NewRouter(),
		config,
	}

	s.middlewares()
	s.context()
	s.routes()

	return &s
}

func (s *ChiServer) Run(port string) error {

	hostname := ":"

	if s.Mode == DebugMode {
		hostname = "127.0.0.1:"
	}

	err := http.ListenAndServe(hostname+port, s.Router)

	return err
}
