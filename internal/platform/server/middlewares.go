package server

import (
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
)

func (s *ChiServer) middlewares() {

	s.Router.Use(middleware.Logger)
	s.Router.Use(middleware.Recoverer)
	s.Router.Use(middleware.Timeout(60 * time.Second))
	s.Router.Use(render.SetContentType(render.ContentTypeJSON))
	s.Router.Use(cors.Handler(corsOptions(s.AllowedOrigin)))
}

func corsOptions(allowedOrigin string) cors.Options {

	return cors.Options{
		AllowedOrigins:   []string{allowedOrigin},
		AllowedMethods:   []string{"GET"},
		AllowedHeaders:   []string{"Accept", "Content-Type"},
		AllowCredentials: false,
		MaxAge:           300,
	}
}
