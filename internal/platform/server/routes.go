package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/nlulic/blog-api/internal/domain/handler"
	"gitlab.com/nlulic/blog-api/pkg/middleware"
)

func (s *ChiServer) routes() {

	s.Router.Route("/posts", func(r chi.Router) {
		r.With(middleware.Paginate(10)).Get("/", posts.GetPosts())
		r.Get("/{postId}", posts.GetPost())
	})
}

func (s *ChiServer) RegisterGetRoute(basePath string) func(string, http.HandlerFunc) {
	return func(path string, handler http.HandlerFunc) {
		s.Router.Route(basePath, func(r chi.Router) {
			r.Get(path, handler)
		})
	}
}

func (s *ChiServer) RegisterPostRoute(basePath string) func(string, http.HandlerFunc) {
	return func(path string, handler http.HandlerFunc) {
		s.Router.Route(basePath, func(r chi.Router) {
			r.Post(path, handler)
		})
	}
}
