package domain

type Author struct {
	Id        int    `json:"id" db:"id"`
	Name      string `json:"name" db:"name"`
	AvatarUrl string `json:"avatarUrl" db:"avatar_url"`
	Username  string `json:"username" db:"username"`
}
