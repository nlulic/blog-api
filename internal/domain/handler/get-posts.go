package posts

import (
	"net/http"
	"strconv"

	"github.com/go-chi/render"
	"gitlab.com/nlulic/blog-api/internal/storage/context"
	"gitlab.com/nlulic/blog-api/pkg/middleware"
)

func GetPosts() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		paging, ok := r.Context().Value(middleware.PAGING_KEY).(middleware.PagingQuery)

		store := context.GetStorage(r.Context())

		if !ok {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		posts, pagination, err := store.GetPosts(paging.Page, paging.PerPage)

		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		// Pagination
		w.Header().Set("Pagination-Page", strconv.Itoa(pagination.Page))
		w.Header().Set("Pagination-Limit", strconv.Itoa(pagination.Total))
		w.Header().Set("Pagination-Total-Pages", strconv.Itoa(pagination.TotalPages))
		w.Header().Set("Pagination-Records", strconv.Itoa(pagination.Records))

		render.JSON(w, r, posts)
	}
}
