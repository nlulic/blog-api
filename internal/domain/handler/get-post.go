package posts

import (
	"database/sql"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/nlulic/blog-api/internal/storage/context"
)

func GetPost() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		postId, _ := strconv.Atoi(chi.URLParam(r, "postId"))

		store := context.GetStorage(r.Context())

		post, err := store.GetPost(postId)

		if err != nil {
			switch err {
			case sql.ErrNoRows:
				http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			default:
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			return
		}

		render.JSON(w, r, post)
	}
}
